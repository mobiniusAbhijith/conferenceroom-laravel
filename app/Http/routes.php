<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',function(){
	return "INVALID_ACCESS";
});



Route::post('register','register@index');

Route::post('login', 'login@index');

Route::post('logout', 'login@logout');

Route::post('addEvent', 'event@add');

Route::post('updateEvent','event@update');

Route::any('deleteEvent/{id}', 'event@delete');

Route::get('getAllUsers', 'users@getAllUsers');

Route::get('getAllEvents','event@getAllEvents');

Route::get('getOneEvent','event@getOneEvent');


// Conference Room Module

Route::post('addConferenceRoom','confroom@addConferenceRoom');

Route::get('getConfRooms','confroom@getConfRooms');

// Route::get('session','register@session');

// Route::get('set_session',function(){
// 	Session::push('status', 1);
// });





// Debugging-------------------------------------
Route::get('r',function(){
	Session::flush();
});

Route::get('s',function(){
	$data = Session::all();
    print_r($data);
});