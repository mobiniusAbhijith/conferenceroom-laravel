<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use MongoClient;

use stdClass;

use Session;

class users extends Controller
{
    //

    public function getAllUsers(Request $request)
    {
    	$response=new stdClass();
    	$users=array();
    	$m = new MongoClient();
	   	$db = $m->ConfRoomBooking;
	    $collection=$db->users;
	  	$documents=$collection->find();
	  	foreach($documents as $document)
	  	{
	  		$userID = $document['_id'];
            $userID=$userID->{'$id'};
	  		$data=array();
	  		$data["Id"]=$userID;
	  		$data["firstName"]=$document['firstName'];
	  		$data["lastName"]=$document['lastName'];
	  		$data["email"]=$document['email'];
	  		$data["mobile"]=$document['mobile'];
	  		$data["dob"]=$document['dob'];
	  		$data["gender"]=$document['gender'];
	  		array_push($users,$data);
	  	}
	  	
	  	$response->user=$users;
	  	$response->success=array("short"=>"","long"=>"");
	  	echo json_encode($response,JSON_PRETTY_PRINT);
    }
}
