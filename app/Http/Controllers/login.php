<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use MongoClient;

use stdClass;

use Session;

class login extends Controller{
    
    public function index(Request $request){

    	$response=new stdClass();

    	if($request->session()->has('user')){
    		
    		$user = Session::get('user');
    		$response->error=array();
    		$response->success=array("sucessMessage"=>"You are already logged in");
    		//$response->user=array("id"=>$user["loginID"][0],"firstName"=>$user["firstName"][0],"lastName"=>$user["lastName"][0]);
 			return response()->json($response);
    	}
    	else{
    		
    		$document["email"]=$request->input("email");
    		$document["password"]=$request->input("password");
    		if($this->validateLogin($document))
    		{
    			$m = new MongoClient();
			   	$db = $m->ConfRoomBooking;
			    $collection=$db->users;
			  	$documents=$collection->find($document);
	    		$exist=$documents->count();
			   	if($exist==1){
			   		$documents->next();
			   		$user=$documents->current();
			   		$obj=$user["_id"];
			   		$userID=$obj->{'$id'};
			   		Session::push('user.loginStatus',1);
	      			Session::push('user.loginID',$userID);
	      			Session::push('user.firstName',$user["firstName"]);
	      			Session::push('user.lastName',$user["lastName"]);
	     	  		$response->error=array();
	    			$response->success=array("sucessMessage"=>"You are successfully logged in");
	    	 		$response->user=array("id"=>$userID,"firstName"=>$user["firstName"],"lastName"=>$user["lastName"],"email"=>$user["email"]);
	 				return response()->json($response);
	      			
			   	}
			   	else{

			   		//$response->error=array("message"=>"invalid credentials");
			   		return response()->json(array("error"=>"Login Credentials are not valid"),500);
			   	}
    		}
            else
            {
                return response()->json();
            }
    	}
    }
    public function logout(Request $request){
    	$response=new stdClass();
    	if($request->session()->has('user')){
    		$request->session()->forget('user');
    		$response->success=array("sucessMessage"=>"You are successfully logged out");
    		return response()->json($response);
    	}
    	else{
    		$response->error=array("message"=>"invalid url");
		   	return response()->json($response);
    	}
    }

    private function validateLogin($document){
    	
    	$response=new stdClass();

    	if($document["email"]==""){
    		$response->message=array("short"=>"E-mail is required.","long"=>"E-mail is required.");
      		$response->isSuccess=0;
      		echo json_encode($response,JSON_PRETTY_PRINT);
      		return false;
    	}
    	elseif(!filter_var($document["email"], FILTER_VALIDATE_EMAIL))
    	{
    		$response->message=array("short"=>"Invalid email format!","long"=>"Invalid email format!");
      		$response->isSuccess=0;
      		echo json_encode($response,JSON_PRETTY_PRINT);
      		return false;
    	}
    	elseif($document["password"]==""){
		    $response->message=array("short"=>"Password is required.","long"=>"Password is required.");
		    $response->isSuccess=0;
		    echo json_encode($response,JSON_PRETTY_PRINT);
		    return false;
		    
    	}else{
    		return true;
    	}
    }
}
