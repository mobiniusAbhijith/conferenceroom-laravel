<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use MongoClient;

use MongoId;

use stdClass;

use Session;


class event extends Controller
{
    
    public function add(Request $request){
    	$response=new stdClass();
    	if($request->session()->has('user')){
    		$user = Session::get('user');
    		$userName=$user["firstName"][0]." ".$user["lastName"][0];
    		$userID=$user["loginID"][0];
    		$document["userID"]=$userID;
    		$document["userName"]=$userName;
	    	$document["eventName"]=trim($request->input("eventName"));
		    $document["eventType"]=trim($request->input("eventType"));
        $date=$request->input("from");
        $document["date"]=date("d-m-Y", strtotime($date));
		    $document["from"]=trim($request->input("from"));
		    $document["to"]=trim($request->input("to"));
		    $document["amenities"]=$request->input("amenities");
		    $document["participantsCount"]=trim($request->input("participantsCount"));
		    $document["confRoomId"]=trim($request->input("confRoomId"));
		    $document["confRoomName"]=trim($request->input("confRoomName"));
		  	if($this->validateEventRegistration($document))
		  	{
		  		$m = new MongoClient();
		   		$db = $m->ConfRoomBooking;
		   		$collection = $db->events;
	   			$collection->insert($document);
	   			$bookingID = $document['_id'];
	      		$bookingID=$bookingID->{'$id'};
	      		$response->success=array("sucessMessage"=>"successfully booked");
	      		$response->eventId=$bookingID;
	      		$response->eventName=$document["eventName"];
	      		$response->eventType=$document["eventType"];
            $response->date=$document["date"];
	      		$response->from=$document["from"];
	      		$response->to=$document["to"];
	      		$response->amenities=$document["amenities"];
	      		$response->participantsCount=$document["participantsCount"];
	      		$response->confRoomId=$document["confRoomId"];
	      		$response->confRoomName=$document["confRoomName"];
	      		$response->userId=$userID;
	      		$response->username=$userName;
	      		return response()->json($response);
		  	}
        else
        {
           return response()->json();
        }

   		}else{

   			$response->error=array("message"=>"Invalid Access. Please login to continue");
		   	return response()->json($response,401);
   		}


    }

    private function validateEventRegistration($document){
    	
    	$response=new stdClass();
    	if($document["eventName"]=="")
    	{
    		$response->message=array("short"=>"Event name is required.","long"=>"Event Name is required.");
      		$response->isSuccess=0;
      		return response()->json($response,400);
      		return false;
    	}
    	elseif($document["eventType"]=="")
    	{
    		$response->message=array("short"=>"Event type is required.","long"=>"Event type is required.");
      		$response->isSuccess=0;
      		echo json_encode($response,JSON_PRETTY_PRINT);
      		return false;
    	}
    	elseif($document["from"]=="")
    	{
    		$response->message=array("short"=>"From is required.","long"=>"From is required.");
      		$response->isSuccess=0;
      		echo json_encode($response,JSON_PRETTY_PRINT);
      		return false;
    	}
    	elseif($document["to"]=="")
    	{
    		$response->message=array("short"=>"To is required.","long"=>"To is required.");
      		$response->isSuccess=0;
      		echo json_encode($response,JSON_PRETTY_PRINT);
      		return false;
    	}
    	elseif(sizeof($document["amenities"])==0)
    	{
    		$response->message=array("short"=>"Amenities  required.","long"=>"Amenities  required.");
      		$response->isSuccess=0;
      		echo json_encode($response,JSON_PRETTY_PRINT);
      		return false;
    	}
    	elseif($document["participantsCount"]=="")
    	{
    		$response->message=array("short"=>"participants count is required.","long"=>"participants count is required.");
      		$response->isSuccess=0;
      		echo json_encode($response,JSON_PRETTY_PRINT);
      		return false;
    	}
    	elseif($document["confRoomId"]=="")
    	{
    		$response->message=array("short"=>"confRoomId is required.","long"=>"confRoomId is required.");
      		$response->isSuccess=0;
      		echo json_encode($response,JSON_PRETTY_PRINT);
      		return false;
    	}
    	elseif($document["confRoomName"]=="")
    	{
    		$response->message=array("short"=>"confRoomName is required.","long"=>"confRoomName is required.");
      		$response->isSuccess=0;
      		echo json_encode($response,JSON_PRETTY_PRINT);
      		return false;
    	}
    	else
    	{
    		return true;
    	}
    }




    public function update(Request $request){

    	$response=new stdClass();
    	if($request->session()->has('user')){

          $eventID=$request->input("eventId");
          if($this->validateEventID($eventID))
          {
              $user = Session::get('user');
              $userName=$user["firstName"][0]." ".$user["lastName"][0];
              $userID=$user["loginID"][0];
              $m = new MongoClient();
              $db = $m->ConfRoomBooking;
              $collection = $db->events;
              $documents=$collection->find(array("_id"=>new MongoId($eventID),"userID"=>$userID));
              $exist=$documents->count();
              if($exist==1)
              {
                  // $condition=array("_id"=>new MongoId($eventID));
                  // $document["eventName"]=trim($request->input("eventName"));
                  // $document["eventType"]=trim($request->input("eventType"));
                  // $document["userID"]=trim($request->input("userId"));
                  // $collection->update($condition,$document);

              }
              else
              {
                  $response->error=array("message"=>"Requested item not found");
                  return response()->json($response,404);
              }
          }
          else
          {
            return response()->json();
          }


    	}else{

        $response->error=array("message"=>"Invalid Access. Please login to continue");
        return response()->json($response,401);
    	}
    }



    public function delete(Request $request,$id){

    	$response=new stdClass();
    	if($request->session()->has('user')){
    		$user = Session::get('user');
    		$bookingID=$id;
    		if($this->validateEventID($bookingID))
    		{
    			$userID=$user["loginID"][0];
	    		//echo $bookingID."<br/>";
	    		//echo $userID;

	    		$m = new MongoClient();
			   	$db = $m->ConfRoomBooking;
			    $collection = $db->events;
			    $documents=$collection->find(array("_id"=>new MongoId($bookingID),"userID"=>$userID));
			    $exist=$documents->count();

			    if($exist==1){
			    	$collection->remove(array("_id"=>new MongoId($bookingID),"userID"=>$userID));
			    	$response->error=array("message"=>"Requested item deleted.");
			   		return response()->json($response);
			    }else{
			    	
			    	$response->error=array("message"=>"Requested item not found");
			   		return response()->json($response,404);
			    }
    		}
        else
        {
          return response()->json();
        }

    	}else{

    		$response->error=array("message"=>"Invalid Access. Please login to continue");
		   	return response()->json($response,401);
    	}

    }

    private function validateEventID($bookingID)
    {
    	$response=new stdClass();
    	if($bookingID==""){
    		$response->error=array("message"=>"Event ID required");
		   	echo json_encode($response,JSON_PRETTY_PRINT);
		   	return false;
    	}
    	else{
    		return true;
    	}
    }


    public function getAllEvents(Request $request)
    {

      $date=$request->input("date");
      $newDate = date("d-m-Y", strtotime($date));
    	$response=new stdClass();
    	$events=array();
    	$m = new MongoClient();
	   	$db = $m->ConfRoomBooking;
	    $collection=$db->events;
	  	$documents=$collection->find(array("date"=>$newDate));
      $exist=$documents->count();
      if($exist>0)
      {
  	  	foreach($documents as $document)
  	  	{
  	  		$eventID = $document['_id'];
          $eventID=$eventID->{'$id'};
  	  		$data=array();
  	  		$data["Id"]=$eventID;
  	  		$data["userName"]=$document['userName'];
          $data["userId"]=$document['userID'];
  	  		$data["eventName"]=$document['eventName'];
  	  		$data["eventType"]=$document['eventType'];
  	  		$data["from"]=$document['from'];
  	  		$data["to"]=$document['to'];
  	  		$data["confRoomName"]=$document['confRoomName'];
  	  		array_push($events,$data);
  	  	}

        $response->events=$events;
        $response->success=array("short"=>"","long"=>"");
      }
      else
      {
        $response->events=[];
      }
	  	
	  	
	  	return response()->json($response);
    }


    public function getOneEvent(Request $request)
    {
    	
    		  
          $eventid=$request->input("eventid");
          if($this->validateEventID($eventid))
          {
            $m = new MongoClient();
            $db = $m->ConfRoomBooking;
            $collection = $db->events;

              $documents=$collection->find(array("_id"=>new MongoId($eventid)));
              $exist=$documents->count();
              if($exist==1)
              {
                $documents->next();
                $event=$documents->current();
                unset($event['_id']);
                $event["eventID"]=$eventid;
                return response()->json($event);
              }
              else
              {
                  return response()->json(array("message"=>"Requested Item not found on server."));
              }
          }
          else
          {
            return response()->json();
          }
    }
}
