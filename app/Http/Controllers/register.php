<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use MongoClient;

use stdClass;

use Session;




class register extends Controller
{

  public function index(Request $request)
  {

    if($request->session()->has('user'))
    {
      $response=new stdClass();
      $response->message=array("short"=>"Error..! Close current session.","long"=>"Error..! Close current session.");
      $response->isSuccess=0;
      echo json_encode($response,JSON_PRETTY_PRINT);
    }
    else
    {
      $document["firstName"]=trim($request->input("firstName"));
      $document["lastName"]=trim($request->input("lastName"));
      $document["mobile"]=trim($request->input("mobile"));
      $document["email"]=trim($request->input("email"));
      $document["password"]=trim($request->input("password"));
      $document["dob"]=trim($request->input("dob"));
      $document["gender"]=trim($request->input("gender"));

      if($this->validateRegistration($document))
      {
            $m = new MongoClient();
            $db = $m->ConfRoomBooking;
            $collection = $db->users;
            $documents=$collection->find(array("email"=>$document["email"]));
            $exist=$documents->count();
            if($exist==0){
            $id=$collection->insert($document);
            $userID = $document['_id'];
            $userID=$userID->{'$id'};
            // session
            Session::push('user.loginStatus',1);
            Session::push('user.loginID',$userID);
            Session::push('user.firstName',$document["firstName"]);
            Session::push('user.lastName',$document["lastName"]);
            // success response
            $response=new stdClass();
            $response->user=array("id"=>$userID,"firstName"=>$document["firstName"],"lastName"=>$document["lastName"],"email"=>$document["email"]);
            $response->message=array("short"=>"Registration success","long"=>"Registration success");
            $response->isSuccess=1;
            //header('Content-Type: application/json');
            echo json_encode($response,JSON_PRETTY_PRINT);
          }
          else{
            $response=new stdClass();
            $response->message=array("short"=>"The email address you have entered is already registered","long"=>"The email address you have entered is already registered. Did you forget your login information <click> here.");
            $response->isSuccess=0;
            header('Content-Type: application/json');
            echo json_encode($response,JSON_PRETTY_PRINT);
          }
      }
    } 
  }

  private function validateRegistration($document){
    
    $response=new stdClass();

    if($document["firstName"]==""){
      $response->message=array("short"=>"First name is required.","long"=>"First name is required.");
      $response->isSuccess=0;
      echo json_encode($response,JSON_PRETTY_PRINT);
      return false;
    }
    elseif(!preg_match ("/^[a-zA-Z\s]+$/",$document["firstName"])){
      $response->message=array("short"=>"First Name must only contain letters!","long"=>"First Name must only contain letters!");
      $response->isSuccess=0;
      echo json_encode($response,JSON_PRETTY_PRINT);
      return false;
    }
    elseif($document["lastName"]==""){
      $response->message=array("short"=>"Last name is required.","long"=>"Last name is required.");
      $response->isSuccess=0;
      echo json_encode($response,JSON_PRETTY_PRINT);
      return false;
    }
    elseif(!preg_match ("/^[a-zA-Z\s]+$/",$document["lastName"])){
      $response->message=array("short"=>"Last Name must only contain letters!","long"=>"Last Name must only contain letters!");
      $response->isSuccess=0;
      echo json_encode($response,JSON_PRETTY_PRINT);
      return false;
    }
    elseif($document["mobile"]==""){
      $response->message=array("short"=>"Mobile Number is required.","long"=>"Mobile Number is required.");
      $response->isSuccess=0;
      echo json_encode($response,JSON_PRETTY_PRINT);
      return false;
    }
    elseif(!preg_match("/^\d{10}$/",$document["mobile"]))
    {
      $response->message=array("short"=>"Invalid Mobile Number!","long"=>"Invalid Mobile Number!");
      $response->isSuccess=0;
      echo json_encode($response,JSON_PRETTY_PRINT);
      return false;
    }
    elseif($document["email"]==""){
      $response->message=array("short"=>"E-mail is required.","long"=>"E-mail is required.");
      $response->isSuccess=0;
      echo json_encode($response,JSON_PRETTY_PRINT);
      return false;
    }
    elseif(!filter_var($document["email"], FILTER_VALIDATE_EMAIL)){
      $response->message=array("short"=>"Invalid email format!","long"=>"Invalid email format!");
      $response->isSuccess=0;
      echo json_encode($response,JSON_PRETTY_PRINT);
      return false;
    }
    elseif($document["password"]==""){
      $response->message=array("short"=>"Password is required.","long"=>"Password is required.");
      $response->isSuccess=0;
      echo json_encode($response,JSON_PRETTY_PRINT);
      return false;
    }
    elseif($document["dob"]==""){
      $response->message=array("short"=>"Date of Birth is required.","long"=>"Date of Birth is required.");
      $response->isSuccess=0;
      echo json_encode($response,JSON_PRETTY_PRINT);
      return false;
    }
    elseif($document["gender"]==""){
      $response->message=array("short"=>"Gender is required.","long"=>"Gender is required.");
      $response->isSuccess=0;
      echo json_encode($response,JSON_PRETTY_PRINT);
      return false;
    }else{

      return true;
    }
  }
}