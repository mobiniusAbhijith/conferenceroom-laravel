<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use MongoClient;

use MongoId;

use stdClass;

use Session;

class confroom extends Controller
{
    


    public function addConferenceRoom(Request $request)
    {
    	$response=new stdClass();
    	$document["cname"]=$request->input("cname");
    	$document["amenities"]=$request->input("amenities");
    	$document["capacity"]=$request->input("capacity");
    	if($this->validateConfRoom($document))
    	{
    		$m = new MongoClient();
	   		$db = $m->ConfRoomBooking;
	   		$collection = $db->confroom;
   			$collection->insert($document);
   			$response->message=array("short"=>"successfully added","long"=>"successfully added.");
      		$response->isSuccess=1;
      		echo json_encode($response,JSON_PRETTY_PRINT);
    	}
    }

    private function validateConfRoom($document)
    {
    	$response=new stdClass();
    	
    	if($document["cname"]=="")
    	{
    		$response->message=array("short"=>"Conference Room Name is required.","long"=>"Conference Room Name is required.");
      		$response->isSuccess=0;
      		echo json_encode($response,JSON_PRETTY_PRINT);
      		return false;
    	}
    	elseif(sizeof($document["amenities"])==0)
    	{
    		$response->message=array("short"=>"Amenities required.","long"=>"Amenities required.");
      		$response->isSuccess=0;
      		echo json_encode($response,JSON_PRETTY_PRINT);
      		return false;
    	}
    	elseif($document["capacity"]=="")
    	{
    		$response->message=array("short"=>"Conference room capacity is required.","long"=>"Conference room capacity is required.");
      		$response->isSuccess=0;
      		echo json_encode($response,JSON_PRETTY_PRINT);
      		return false;
    	}
    	else
    	{
    		return true;
    	}

    }



    public function getConfRooms(Request $request)
    {
    	$response=new stdClass();
    	$m = new MongoClient();
   		$db = $m->ConfRoomBooking;
   		$collection = $db->confroom;
   		$documents=$collection->find();
   		$exist=$documents->count();

		if($exist==0){
			$response->message=array("short"=>"No data found","long"=>"No data found");
      		$response->isSuccess=0;
      		echo json_encode($response,JSON_PRETTY_PRINT);
      		return true;
		}
		else{
			$confroom=array();

			foreach($documents as $document)
		  	{
		  		$confRoomID = $document['_id'];
	            $confRoomID=$confRoomID->{'$id'};
		  		$data=array();
		  		$data["Id"]=$confRoomID;
		  		$data["cname"]=$document['cname'];
		  		$data["amenities"]=$document['amenities'];
		  		$data["capacity"]=$document['capacity'];
		  		array_push($confroom,$data);
		  	}
		  	
		  	$response->confroom=$confroom;
		  	//$response->success=array("short"=>"","long"=>"");
		  	echo json_encode($response,JSON_PRETTY_PRINT);
		}
    }
}
